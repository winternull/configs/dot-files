# $FreeBSD$
#-----------------------------------------------------------------------------#
# File: .cshrc
# Date: 2023-04-13
# Version: 2.2.6
# Ref: /usr/share/examples/csh/
#-----------------------------------------------------------------------------#
# setenv LC_ALL en_US.UTF-8
# Check language/charset for user in /etc/login.conf
#   execute `sudo cap_mkdb /etc/login.conf` if changes are made
#-----------------------------------------------------------------------------#
#
#-----------------------------------------------------------------------------#
#set HIST="~/.history"
#set filec;
#set autorehash;
#set autoexpand;
#set history = 8192
#set histdup = erase
#set savehist = (8192 merge lock)
#set savehist = (${history} merge lock)
#-----------------------------------------------------------------------------#
#alias precmd 'history -L, history -S'
#alias postcmd 'history -M'
#-----------------------------------------------------------------------------#
#alias precmd 'history -S; history -M'
#if (`wc --libxo text -l ${HIST} | cut -d '/' -f1` == 2000) then
#        mv ${HIST} ${HIST}.old
#        touch ${HIST} && chmod 600 ${HIST}
#endif
#-----------------------------------------------------------------------------#
alias redshift-v0 'redshift -v -o -P -c /usr/local/etc/redshift/rev0.conf'
alias redshift-v1 'redshift -v -o -P -c /usr/local/etc/redshift/rev1.conf'
alias redshift-v2 'redshift -v -o -P -c /usr/local/etc/redshift/rev2.conf'
alias redshift-v3 'redshift -v -o -P -c /usr/local/etc/redshift/rev3.conf'
alias redshift-v4 'redshift -v -o -P -c /usr/local/etc/redshift/rev4.conf'
#-----------------------------------------------------------------------------#
alias firefox-xapp    'ssh -X xapp-browser "firefox --no-remote"'
#-----------------------------------------------------------------------------#
alias hist            'history -h | egrep --no-messages --mmap --ignore-case'
alias histsearch      'echo "searching $USER + root shell histories..." && \
                           sudo (grep -i "$1" \
                             {~/,/root/}.bash_history \
                             {~/,/root/}.local/share/fish/fish_history \
                             {~/,/root/}.history \
                           )'
#-----------------------------------------------------------------------------#
alias jls             'sudo jls -vd; sudo bastille list --all;'
#-----------------------------------------------------------------------------#
alias ls              'ls -BFG -TW'
alias l               'ls -l'
alias la              'ls -a'
alias lf              'ls -A'
alias ll              'ls -lA'
alias lh              'ls -lAh'
#-----------------------------------------------------------------------------#
alias c               '/usr/local/bin/ccat'
alias cat             '/usr/local/bin/ccat'
alias tree            'tree -apugh --du'
alias dh              'sudo du -d 1 -h $1 | sort -h'
alias grep            'egrep -SinU --no-messages --mmap --color=always'
alias messages        'sudo tail -f /var/log/all.log | egrep -vi "pfstat"\|"atrun"\|"save-entropy" | lnav'
#-----------------------------------------------------------------------------#
alias no_comments     'sudo grep "^[^#;]" $1'
alias no_comments_cmd 'echo "^[^#;]"'
#-----------------------------------------------------------------------------#
alias mk               'eval ${1:-MKPWD}=\"`pwd`\"'
alias rt               'eval cd \"\$${1:-MKPWD}\"'
#-----------------------------------------------------------------------------#
alias pkg-add         'sudo pkg install $@'
alias pkg-clean       'sudo pkg clean all'
alias pkg-del         'sudo pkg remove $@'
alias pkg-info        'pkg info $@'
alias pkg-remove      'sudo pkg remove $@'
alias pkg-search      'pkg search $@'
alias pkg-update      'sudo pkg update -f'
alias pkg-upgrade     'sudo pkg upgrade $@'
alias pkg-downgrade   'sudo pkg downgrade $@'
#-----------------------------------------------------------------------------#
# These are normally set through /etc/login.conf.  You may override them here
# path = /sbin /bin /usr/sbin /usr/bin /usr/local/sbin /usr/local/bin $HOME/bin
# umask 22
setenv  EDITOR  vim
setenv  PAGER   less
#-----------------------------------------------------------------------------#
if ($?prompt) then
    # An interactive shell -- set some stuff up
    set HIST="~/.history"
    set prompt = "%N@%m:%~ %# "
    set promptchars = "%#"
    set filec
    set history = 65534
    set savehist = (1000 merge)
    set autolist = ambiguous
    set autoexpand
    set autorehash
    set mail = (/var/mail/$USER)
    if ( $?tcsh ) then
        bindkey "^W" backward-delete-word
        bindkey -k up history-search-backward
        bindkey -k down history-search-forward
    endif
endif
#-----------------------------------------------------------------------------#
