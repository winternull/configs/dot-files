-- Name     : .conkyrc
-- Purpose  : Config file for Conky app
-- Author   : EM Winterschon
-- Repo     :
-- Requires : Conky installed
-- Date     : 2023-12-10
-- Version  : 2.2
-- Compat   : Conky 1.19.2

conky.config = {
	use_spacer = 'left',
	pad_percents = 3,
	background = false,
	double_buffer = true,
	use_xft = true,
	font = 'monospace:size=7',

  -- [start] use the following for a double monitor: 4K top + 4K lower
  alignment = 'top_right',
  gap_x = 10,
  gap_y = 10,
  -- [end]

  own_window = true,
  --own_window_type = desktop,
  own_window_argb_visual = true,
  own_window_argb_value = 0,
  --own_window_hints = 'above,skip_taskbar,skip_pager,sticky',
  --own_window_hints = 'above,undecorated,below,skip_taskbar,skip_pager,sticky',
  own_window_hints = 'undecorated,below,skip_taskbar,skip_pager,sticky',
  update_interval = 5.0,
  lua_load = "/home/eva/.local/bin/ping.lua",
};

conky.text = [[

${color magenta}Hostname: ${color}${nodename}
${color magenta}NOW():    ${color}${time %A %F %T %z %Z}
${color magenta}Kernel:   ${color}${sysname} ${kernel}
${color magenta}Uptime:   ${color}${uptime}
#${color magenta}Profile:  ${color}${texeci 300 tuned-adm profile_info | head -n 2 | tail -n 1}
${hr}
${color magenta}CPU TST:  ${color} ${freq_g} GHz T-State
${color magenta}CPU SKU:  ${color} ${execi 300 lscpu | grep "Model name:" | awk '{print $3,$4,$6,$9}' | tr -d '()' | sed 's/R//g'}
${color magenta}CPU Temp: ${color} ${execi 60 sudo ipmitool sdr | grep -Ei "CPU Temp" | awk '{print $4"C"}'}
${color magenta}CPU Fans: ${color} ${execi 120 sudo ipmitool sdr | grep -Ei "FAN1" | awk '{print $3,$4}'}
${color magenta}PCIe Fans:${color} ${execi 120 sudo ipmitool sdr | grep -Ei "FANA" | awk '{print $3,$4}'}, ${execi 120 sudo ipmitool sdr | grep -Ei "FANB" | awk '{print $3,$4}'}
${hr}
#${color magenta}SUM${color}${cpu cpu0}% ${cpubar cpu0 6,200}
${color magenta}01:${color}${cpu cpu1}% ${cpubar cpu1 6,100} ${color magenta}17:${color}${cpu cpu17}% ${cpubar cpu17 6,100}
${color magenta}02:${color}${cpu cpu2}% ${cpubar cpu2 6,100} ${color magenta}18:${color}${cpu cpu18}% ${cpubar cpu18 6,100}
${color magenta}03:${color}${cpu cpu3}% ${cpubar cpu3 6,100} ${color magenta}19:${color}${cpu cpu19}% ${cpubar cpu19 6,100}
${color magenta}04:${color}${cpu cpu4}% ${cpubar cpu4 6,100} ${color magenta}20:${color}${cpu cpu20}% ${cpubar cpu20 6,100}
${color magenta}05:${color}${cpu cpu5}% ${cpubar cpu5 6,100} ${color magenta}21:${color}${cpu cpu21}% ${cpubar cpu21 6,100}
${color magenta}06:${color}${cpu cpu6}% ${cpubar cpu6 6,100} ${color magenta}22:${color}${cpu cpu22}% ${cpubar cpu22 6,100}
${color magenta}07:${color}${cpu cpu7}% ${cpubar cpu7 6,100} ${color magenta}23:${color}${cpu cpu23}% ${cpubar cpu23 6,100}
${color magenta}08:${color}${cpu cpu8}% ${cpubar cpu8 6,100} ${color magenta}24:${color}${cpu cpu24}% ${cpubar cpu24 6,100}
${color magenta}09:${color}${cpu cpu9}% ${cpubar cpu9 6,100} ${color magenta}25:${color}${cpu cpu25}% ${cpubar cpu25 6,100}
${color magenta}10:${color}${cpu cpu10}% ${cpubar cpu10 6,100} ${color magenta}26:${color}${cpu cpu26}% ${cpubar cpu26 6,100}
${color magenta}11:${color}${cpu cpu11}% ${cpubar cpu11 6,100} ${color magenta}27:${color}${cpu cpu27}% ${cpubar cpu27 6,100}
${color magenta}12:${color}${cpu cpu12}% ${cpubar cpu12 6,100} ${color magenta}28:${color}${cpu cpu28}% ${cpubar cpu28 6,100}
${color magenta}13:${color}${cpu cpu13}% ${cpubar cpu13 6,100} ${color magenta}29:${color}${cpu cpu29}% ${cpubar cpu29 6,100}
${color magenta}14:${color}${cpu cpu14}% ${cpubar cpu14 6,100} ${color magenta}30:${color}${cpu cpu30}% ${cpubar cpu30 6,100}
${color magenta}15:${color}${cpu cpu15}% ${cpubar cpu15 6,100} ${color magenta}31:${color}${cpu cpu31}% ${cpubar cpu31 6,100}
${color magenta}16:${color}${cpu cpu16}% ${cpubar cpu16 6,100} ${color magenta}32:${color}${cpu cpu32}% ${cpubar cpu32 6,100}

${cpugraph}
#${color magenta}Memory:         Used/Total          Free          Swap Used/Total
${color}         ${mem}/${memmax}      ${memfree}        ${swap}/${swapmax}
${memperc}% ${membar 4}
${memgraph}
#${color magenta}Load average: ${color}${loadavg}     ${color magenta}Processes: ${color}${processes}   ${color magenta}Running:${color} ${running_processes}
${color magenta}Name              PID     CPU%   MEM%
${color lightgrey} ${top name 1} ${top pid 1} ${top cpu 1} ${top mem 1}
${color lightgrey} ${top name 2} ${top pid 2} ${top cpu 2} ${top mem 2}
${color lightgrey} ${top name 3} ${top pid 3} ${top cpu 3} ${top mem 3}
${color lightgrey} ${top name 4} ${top pid 4} ${top cpu 4} ${top mem 4}
${color lightgrey} ${top name 5} ${top pid 5} ${top cpu 5} ${top mem 5}
${hr}
${color magenta}IPaddr    Public:    ${color lightgrey}${texeci 60 curl --silent --fail https://ipecho.net/plain}, ${tcp_ping ipecho.net 443} ms
${hr}
${color magenta}LOC-INT  | ${color lightgrey}RFC-000  | HYP-GOG  | HYP-IBM
${color magenta}NET-PORT | ${color lightgrey}LAN-443  | WAN-443  | QUAD-9D
${color magenta}RTT-TIME | ${color lightgrey}${goto 65}${tcp_ping 172.16.99.1 443} ms ${goto 110}| ${color lightgrey}${tcp_ping google.com 443} ms | ${color lightgrey}${tcp_ping 9.9.9.9 53} ms

#${color magenta}WAN-SSH  | SFO-200 |    || ${color magenta}LAN-SSH | EX3300 | ${color lightgrey}${texeci 60 ~/.local/bin/pingtest.sh 172.16.99.5}, ${tcp_ping 172.16.99.5 22} ms
#${color magenta}WAN-DNS  | 8.8.8.8 | ${color lightgrey}${texeci 30 ~/.local/bin/pingtest.sh 8.8.8.8}, ${tcp_ping 8.8.8.8 53} ms   || ${color magenta}WAN-DNS | 9.9.9.9 | ${color lightgrey}${texeci 30 ~/.local/bin/pingtest.sh 9.9.9.9}, ${tcp_ping 9.9.9.9 53} ms
${hr}
${color magenta}Mount               Used / Total
${color magenta}/               ${color}${fs_used /} / ${fs_size /} ${fs_bar 6,100 /}
${color magenta}/tmp[fs]        ${color}${fs_used /tmp} / ${fs_size /tmp} ${fs_bar 6,100 /tmp}
${color magenta}/home[fs]        ${color}${fs_used /home} / ${fs_size /home} ${fs_bar 6,100 /home}
${color magenta}ports[fs]  ${color}${fs_used /usr/ports} / ${fs_size /usr/ports} ${fs_bar 6,100 /usr/ports}
${hr}
${color magenta}ZFS Pools
${color}${texeci 10 zpool list | head -n 1 | awk '{print $1"\t\t"$2"\t"$3"\t"$4"\t"$7"\t"$9}'}
${color}${texeci 10 zpool list | head -n 2 | tail -n 1 | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$7"\t"$9}'}
${color}${texeci 10 zpool list | head -n 3 | tail -n 1 | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$7"\t"$9}'}
${hr}
${color magenta}EST -3: ${color lightgrey}${tztime America/New_York} | ${color magenta}CST -2: ${color lightgrey}${tztime US/Central}
${color magenta}PST -7: ${color lightgrey}${tztime US/Pacific} | ${color magenta}UTC -0: ${color lightgrey}${tztime UTC}
${hr}
${acpifan}
${acpitemp}
]];

