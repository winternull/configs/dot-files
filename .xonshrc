#!/usr/bin/env xonsh
# --------------------------------------------------------------------------- #
# File:    ~/.xonshrc
# Purpose: configuration for XonSH shell
# Version: 0.8-4
# Date:    2024-0428
# Author:  em-winterschon
# Ref:     https://xonsh.github.io
# --------------------------------------------------------------------------- #

# --------------------------------------------------------------------------- #
# General XonSH internals
# --------------------------------------------------------------------------- #
# - https://xon.sh/customization.html
# - https://xon.sh/envvars.html#bash-completions
# - https://xon.sh/envvars.html#prompt-toolkit-color-depth
# - https://xon.sh/python_virtual_environments.html
# --------------------------------------------------------------------------- #
$TERM="xterm-256color"
$COLORSCHEME="simple"
$XONSH_COLOR_STYLE="vim"
#
$COMPLETION_MODE = 'menu-complete'
$COMPLETIONS_CONFIRM = False
$COMPLETIONS_DISPLAY = 'readline'
$COMPLETIONS_MENU_ROWS = 4
$COMPLETION_IN_THREAD = True
$AUTO_SUGGEST = False
$AUTO_SUGGEST_IN_COMPLETIONS = False
$UPDATE_COMPLETIONS_ON_KEYPRESS = False
$CASE_SENSITIVE_COMPLETIONS = True
$CMD_COMPLETIONS_SHOW_DESC = False
$FUZZY_PATH_COMPLETION = False

# --------------------------------------------------------------------------- #
# Virtual Environments - Vox
# --------------------------------------------------------------------------- #
xontrib load vox
$VIRTUALENV_HOME = "/home/eva/.xsh/venvs"
$VOX_DEFAULT_INTERPRETER = "/usr/local/bin/python3.11"

# --------------------------------------------------------------------------- #
# Debugging & Traceback Happyplace
# --------------------------------------------------------------------------- #
xontrib load readable-traceback
$XONSH_SHOW_TRACEBACK = True
$XONSH_TRACE_COMPLETIONS = False
$XONSH_TRACEBACK_LOGFILE = "/home/eva/.xsh/logs/traceback.buffer.log"
$READABLE_TRACE_STRIP_PATH_ENV=True
$READABLE_TRACE_REVERSE=True

# do you need> set -e
# can use this> $RAISE_SUBPROC_ERROR = True

# how about> set +x
# here you go> $XONSH_TRACE_SUBPROC = True

# History Backend
# --------------------------------------------------------------------------- #
$XONSH_HISTORY_SIZE = '128 megabytes'
$XONSH_DATA_DIR = "/home/eva/.xsh/data"
$XONSH_HISTORY_BACKEND = "json"
$XONSH_CAPTURE_ALWAYS = True
$XONSH_STORE_STDOUT = False

# History encryption / obfuscation
# --------------------------------------------------------------------------- #
#$XONSH_HISTORY_ENCRYPTOR = 'json'
#xontrib load history_encrypt

# --------------------------------------------------------------------------- #
# Imports + ReDefs
# --------------------------------------------------------------------------- #
import io
import sys
from pathlib import Path
from xonsh.prompt.base import PromptField, PromptFields
from prompt_toolkit.keys import Keys
from prompt_toolkit.filters import Condition, EmacsInsertMode, ViInsertMode
from xonsh.tools import unthreadable, uncapturable

# disable buffering outputs
sys.stdout = io.TextIOWrapper(open(sys.stdout.fileno(), "wb", 0), write_through=True)
sys.stderr = io.TextIOWrapper(open(sys.stderr.fileno(), "wb", 0), write_through=True)

# --------------------------------------------------------------------------- #
# SSH-AGENT AUTH-SOCK
# --------------------------------------------------------------------------- #
#_SSH_AGENT_ENV=Path($XDG_RUNTIME_DIR + "/ssh-agent.env")
#if not $(pgrep -u $USER ssh-agent):
#    _SSH_AGENT_ENV.write_text($(ssh-agent -s))
#else:
#  if _SSH_AGENT_ENV.exists():
#      source-bash @(_SSH_AGENT_ENV)
#  else:
#      echo "ssh-agent running but _SSH_AGENT_ENV not found..."
#      echo "-> suggest running: pkill -9 ssh-agent"

# --------------------------------------------------------------------------- #
# Dynamic prompt generation
# --------------------------------------------------------------------------- #
$PROMPT_TOOLKIT_COLOR_DEPTH = 'DEPTH_24_BIT'
$XONSH_DATETIME_FORMAT="%y·%m%d·%H%M,%S"
$DYNAMIC_CWD_WIDTH='80'
$DYNAMIC_CWD_ELISION_CHAR='^'

$PROMPT_FIELDS['time_format'] = $XONSH_DATETIME_FORMAT
$PROMPT_FIELDS['prompt_end'] = '»'

# prompt field customized variables
# ─────────────────────────────────────|
# 1. to change the color of the branch name
# 2. to change the symbol for conflicts from ``{RED}×``
# 3. hide the branch name if it is main or master

$PROMPT_FIELDS['gitstatus.branch'].prefix = "{RED}"
$PROMPT_FIELDS['gitstatus.conflicts'].prefix = "{GREEN}*"
branch_field = $PROMPT_FIELDS['gitstatus.branch']

old_updator = branch_field.updator
def new_updator(fld: PromptField, ctx: PromptFields):
    old_updator(fld, ctx)
    if fld.value in {"main", "master"}:
        fld.value = ""
branch_field.updator = new_updator

# custom color stuff
# ─────────────────────────────────────|
# from xonsh.tools import register_custom_style
# zcolors = {
#     "Literal.String.Single": "#ff88aa",
#     "Literal.String.Double": "#ff4488",
#     "RED": "#008800",
# }
# register_custom_style("zcolors", zcolors, base="monokai")
# $XONSH_COLOR_STYLE="zcolors"

# setting up the prompt
# ─────────────────────────────────────|
$PROMPT = '{INTENSE_BLACK}┬─[{BOLD_PURPLE}{user}{WHITE}Ø{PURPLE}{hostname}{RESET}{INTENSE_BLACK}]─[{BOLD_RED}{localtime}{INTENSE_BLACK}]─[(rc:{WHITE}{last_return_code}{BLUE}){INTENSE_BLUE}{curr_branch:§{}}{INTENSE_BLACK}]{env_name:{}}\n{WHITE}§{cwd}\n{INTENSE_CYAN}{RESET}╰─{INTENSE_BLACK}»{prompt_end}{RESET} '


# --------------------------------------------------------------------------- #
# Custom Functions & Variables
# --------------------------------------------------------------------------- #
# TODO: convert bar40, bar80 into functions which print the required count
bar40 = "# ─────────────────────────────────────|"
bar80 = "# ----------------------------------------------------------------------------|"

# Testing / Example Functions
# Exec sub-proc call w/ stdout capture
# ─────────────────────────────────────|
@unthreadable
def _exp_fg(args, stdin=None):
    return 'unthreaded echo alias here'

# Exec sub-proc call w-o stdout capture
# ─────────────────────────────────────|
@uncapturable
@unthreadable
def _exp_no_out(*args, **kwargs):
    vim

@unthreadable
def print_bar40():
    print(bar40)

@unthreadable
def print_bar80():
    print(bar80)

# Editor Selector
@unthreadable
def xsh_editor(stdin=None):
    # TODO: check path existence for "micro", "vim", "vi" in sequence \
    #   .. and assign variable according to binary availability
    _editor = "micro"
    return _editor

# Simple File Check Func
def check_file(file):
    if !(test -e @(file)):
        if !(test -f @(file)) or !(test -d @(file)):
            print("File is a regular file or directory")
        else:
            print("File is not a regular file or directory")
    else:
        print("File does not exist")

# --------------------------------------------------------------------------- #
# Keybindings
# --------------------------------------------------------------------------- #
@events.on_ptk_create
def custom_keybindings(bindings, **kw):
    # Keys.ControlW = same as = c-w

    @bindings.add(Keys.ControlW)
    def print_bar40(event):
        event.current_buffer.insert_text("%s" %(bar40))

    @unthreadable
    @uncapturable
    @bindings.add(Keys.ControlP)
    def run_ls(event):
        ls -l
        event.cli.renderer.erase()

# --------------------------------------------------------------------------- #
# Command Aliases and Abbrevs
# - https://xon.sh/tutorial.html#aliases
# --------------------------------------------------------------------------- #
xontrib load abbrevs
# ─────────────────────────────────────|

abbrevs['..'] = 'cd ..'
abbrevs['...'] = 'cd ../..'
abbrevs['c'] = 'ccat'
abbrevs['e'] = xsh_editor()
abbrevs['f'] = 'fetch'
abbrevs['g'] = 'git'
abbrevs['s'] = 'sudo'
abbrevs['p'] = 'ping'

abbrevs['cat'] = 'ccat'
abbrevs['cx'] = 'cd ..'
abbrevs['envgrep'] = 'env | grep'
abbrevs['follow'] = 'sudo tail -f'
abbrevs['help-debug'] = '${...}.help("XONSH_DEBUG")'
abbrevs['pkg'] = 'sudo pkg'
abbrevs['sp'] = 'sudo nping'
abbrevs['sss'] = 'sudo -s'

aliases['exp_fg'] = _exp_fg
aliases['exp_no_out'] = _exp_no_out
aliases['bar40'] = print_bar40
aliases['dh'] = 'sudo du -d1 -h'
aliases['clip'] = 'xclip -sel clip'
aliases['qqs'] = 'pkg search'
aliases['qqi'] = 'sudo pkg inst'
aliases['fbsd-update-fetch'] = 'sudo freebsd-update fetch'
aliases['fbsd-update-install'] = 'sudo freebsd-update install'

aliases['g-pull'] = 'git pull'
aliases['g-commit'] = 'git commit -S'
aliases['g-push'] = 'git push'
aliases['g-fetch'] = 'git fetch'
aliases['g-merge'] = 'git merge -S'

aliases['imgconv.webp-2-png'] = 'magick mogrify -format png *.webp *.WEBP'
aliases['imgconv.jpeg-2-png'] = 'magick mogrify -format png *.jpg *.JPG *.jpeg *.JPEG'
aliases['imgconv.heic-2-png'] = 'magick mogrify -format png *.heic *.HEIC'

aliases['rc_edit'] = ('sudo %s /etc/rc.conf' % xsh_editor())
aliases['xsh_edit'] = ('%s ~/.xonshrc' % xsh_editor())
aliases['xsh_bkup'] = 'cp ~/.xonshrc ~/.xonshrc.bak'
aliases['rsync'] = 'BINARY_SSH=rsync /usr/local/bin/repassh'
aliases['scp'] = 'BINARY_SSH=scp /usr/local/bin/repassh'
aliases['ssh'] = '/usr/local/bin/repassh'

aliases['ipmi-109'] = 'ipmitool -e . -H 172.16.199.109 -U root -P calvin -I lanplus'

# args need to be variablized
# https://xon.sh/tutorial.html#callable-aliases
# aliases['fident'] = '!(Q="ARG1" && ll "$Q" && file "$Q")'

aliases['l'] = 'eza --time-style long-iso --octal-permissions --all --group-directories-first --oneline --long --classify --icons --color-scale --color=always --sort name --header --binary --group --git'
aliases['ll'] = 'l'
aliases['lh'] = 'l'

aliases['recv'] = 'rsync -ai --stats '
aliases['send'] = 'rsync -ai --stats '
#aliases['xconsole'] = 'sudo xconsole -exitOnFail -notify -f /dev/xconsole'

# --------------------------------------------------------------------------- #
# Xontribs -- Other
# --------------------------------------------------------------------------- #
xontrib load argcomplete
xontrib load fish_completer  # <--- this is better than the bash_completer
xontrib load coreutils
xontrib load pdb
xontrib load jedi
xontrib load gitinfo
xontrib load kitty
xontrib load xog
#xontrib load ssh_agent #<--- this is not happy, we don't need it

# Stupid sh/bash stuff
# --------------------------------------------------------------------------- #
#$XONTRIB_SH_SHELLS = ['bash', 'sh', 'fish']
$XONTRIB_SH_SHELLS = ['sh', 'fish']
$XONTRIB_SH_USEFULL = True
$XONTRIB_SH_USEFIRST = False
xontrib load sh

# Output search == 'ALT + i'
# --------------------------------------------------------------------------- #
# Search command output strings: alt + i
$XONTRIB_OUTPUT_SEARCH_KEY='i'
xontrib load output_search

# Onepath xontrib might be causing issues - debug later
# --------------------------------------------------------------------------- #
# xontrib load onepath
# $XONTRIB_ONEPATH_DEBUG = False                       # enable/disable debug mode
# $XONTRIB_ONEPATH_ACTIONS['text/'] = 'micro'          # editor for generic text files
# $XONTRIB_ONEPATH_ACTIONS['.xonshrc'] = 'micro'       # editor for `.xonshrc` file
# $XONTRIB_ONEPATH_ACTIONS['*.log'] = 'tail'           # tail for text type *.log files
# $XONTRIB_ONEPATH_ACTIONS['text/plain.txt'] = 'less'  # less for plain text *.txt files
# $XONTRIB_ONEPATH_ACTIONS['<DIR>'] = 'ls'             # list the files in the directory
# $XONTRIB_ONEPATH_ACTIONS['application/zip'] = 'als'  # list files in zip file using atool

# OpenAI Chat integration
# --------------------------------------------------------------------------- #
# - chat-manager help
# - https://github.com/jpal91/xontrib-chatgpt
#$OPENAI_API_KEY = '<your api key>'            # default: not set
#$OPENAI_CHAT_MODEL = 'gpt-3.5-turbo'          # opt: gpt-3.5-turbo, gpt-4
#xontrib load chatgpt

# --------------------------------------------------------------------------- #
# Coloured man page support
# using 'less' env vars (format is '\E[<brightness>;<colour>m')
# --------------------------------------------------------------------------- #
$LESS_TERMCAP_mb = "\033[01;31m"     # begin blinking
$LESS_TERMCAP_md = "\033[01;31m"     # begin bold
$LESS_TERMCAP_me = "\033[0m"         # end mode
$LESS_TERMCAP_so = "\033[01;44;36m"  # begin standout-mode (bottom of screen)
$LESS_TERMCAP_se = "\033[0m"         # end standout-mode
$LESS_TERMCAP_us = "\033[00;36m"     # begin underline
$LESS_TERMCAP_ue = "\033[0m"         # end underline

# --------------------------------------------------------------------------- #
# Module Repos + xPip Install Commands
# --------------------------------------------------------------------------- #
# https://github.com/xonsh/xontrib-abbrevs
# https://github.com/anki-code/xontrib-argcomplete
# https://github.com/xonsh/xontrib-debug-tools
# https://github.com/xxh/xxh
# https://github.com/anki-code/xontrib-history-encrypt
# https://github.com/jnoortheen/xontrib-hist-navigator
# https://github.com/xonsh/xontrib-jedi
# https://github.com/vaaaaanquish/xontrib-readable-traceback
# https://github.com/anki-code/xontrib-sh
# https://github.com/dyuri/xontrib-gitinfo
# https://github.com/jnoortheen/xontrib-cmd-durations
# https://github.com/anki-code/xontrib-output-search
# https://github.com/anki-code/xontrib-clp
# https://github.com/anki-code/xontrib-onepath
# https://github.com/scopatz/xontrib-kitty
# https://github.com/jpal91/xontrib-chatgpt
# https://github.com/xonsh/xontrib-fish-completer
# https://github.com/dyuri/xontrib-ssh-agent
# https://github.com/xonsh/xontrib-vox

# Note: Xontribs installed as System vs User
#  - if a xontrib module won't load despite xpip having installed it
#    and the location is listed similar to '/usr/local/lib/python3.N/site-packages/...'
#    then it may require removal or managing the include from a --user space install
#
# xpip install -U xontrib-debug-tools
# xpip install -U xxh-xxh
# xpip install -U xontrib-history-encrypt
# xpip install -U xontrib-abbrevs
# xpip install -U xontrib-jedi
# xpip install -U xontrib-readable-traceback
# xpip install -U xontrib-sh
# xpip install -U xontrib-gitinfo && sudo pkg install -y onefetch
# xpip install -U xontrib-kitty
# xpip install -U xontrib-argcomplete
# xpip install -U xontrib-output-search
# xpip install -U xontrib-chatgpt
# xpip install -U xontrib-fish-completer
# xpip install -U xontrib-ssh-agent
# xpip install -U xontrib-vox
#
# --------------------------------------------------------------------------- #
# END
# --------------------------------------------------------------------------- #