if status is-interactive
    # Commands to run in interactive sessions can go here
    # ------------------------------------------------------ #
    starship init fish | source
    #
    # Aliases, Functions, Environment Variables
    # ------------------------------------------------------ #
    # Are the following commands useful?
    #   switch $argv[(count $argv)]
    # ------------------------------------------------------ #
    #
    set -gx EDITOR vim
    set -gx PAGER less

    # Porting the 'netinfo' function from superbash may take a bit
    function netinfo --description 'display network information'
        echo "All Interfaces"
        ifconfig
        echo "Active Interfaces"
        ifconfig | pcregrep -M -o '^[^\t:]+:([^\n]|\n\t)*status: active'
    end

    # Services: command management wrapper
    function sctl --wraps service --description 'service controls via standardized commands across *nixes'
        if test (count $argv) -lt 2
            echo "ARGV missing from command: " \
                "Usage: sysctl <ACTION> <SERVICE>" \
                    "action: <(one)start, (one)stop, (one)restart, status, enable, disable, list-all, list-active, list-enabled" \
                "service: <process-name eg: nginx apache>"
            return
        end

        set act $argv[1]      # -> action @name
        set svc $argv[2]      # -> service @name
        switch $act
            case start stop status onestart onestop onestatus enable disable
                sudo service $svc $act
                set -x retcode $status
        end

        if test $retcode -gt 0
            echo "[ERROR]: failure on action [$act], retcode [$retcode]"
        else
            echo "[OK]: success on action [$act]. retcode [$retcode]"
        end
    end

    #### Porting from Bash
    function extract
        if test (count $argv) -lt 1
            echo "ARGV1 missing from command: " \
                "Usage: extract <FILE>"
            return
        end

        set f $argv[1]
        switch $f
            case *.tar.bz2
                tar xvjf $f
            case *.tar.gz
                tar xvzf $f
            case *.bz2
                bunzip2 $f
            case *.rar
                unrar x $f
            case *.gz
                which pigz 2&1 > /dev/null
                if test $status -gt 0
                    gunzip $f
                else
                    pigz $f
                end
            case *.tar
                tar xvf $f
            case *.tbz2
                tar xvjf $f
            case *.tgz
                tar xvzf $f
            case *.zip
                unzip $f
            case *.Z
                uncompress $f
            case *.7z
                7zz x $f
            case *
                echo "'$f' cannot be extracted via >>-extract-<<"
        end
    end
    #### END

    # Files: list w/ tree format
    function trees --wraps tree --description 'display file+dirs in tree format with flags'
        trees -apugh --du $argv
    end

    # Disk: report w/ space usage, one level deep
    function dh --wraps du --description 'list directory sizes, ordered depth=1'
        sudo du -d 1 -h $argv | sort -h
    end

    # Logging: system all.log w/ explicit ignore on repetitive entries
    function messages --wraps tail --description 'follow all.log w/o egreps + lnav'
        sudo tail -f /var/log/all.log | egrep -vi "pfstat"\|"atrun"\|"save-entropy" | lnav
    end

    # Files: display contents w/o hash or empty space at start of line
    function no-comments --wraps grep --description 'display file w/o "#" commented lines'
        sudo grep "^[^#;]" $argv
    end

    # GUI: set redshift w/ different levels of config for 'DISPLAY=unix:0.0'
    function redshift-conf --wraps redshift --description "wrapper for redshift, offering multiple levels of darkness configuration"
        printf %s\n \
            "-------------------------" \
            " Redshift Configurations" \
            "-------------------------"
        if test (count $argv) -lt 1
            echo "ARGV1 missing from command: " \
                "Usage: redshift-conf set-conf-[0..3]"
            return
        end
        switch $argv[1]
            case set-conf-0
                redshift -v -o -P -c /usr/local/etc/redshift/rev0.conf
                set -x retcode $status

            case set-conf-1
                redshift -v -o -P -c /usr/local/etc/redshift/rev1.conf
                set -x retcode $status

            case set-conf-2
                redshift -v -o -P -c /usr/local/etc/redshift/rev2.conf
                set -x retcode $status

            case set-conf-3
                redshift -v -o -P -c /usr/local/etc/redshift/rev3.conf
                set -x retcode $status

            case set-conf-4
                redshift -v -o -P -c /usr/local/etc/redshift/rev4.conf
                set -x retcode $status
        end

        if test $retcode -gt 0
            echo "[ERROR]: Failed to set redshift configuration level [$argv[1]]"
        else
            echo "[SUCCESS]: redshift configuration level [$argv[1]]"
        end
    end

    # IPMI Tool Aliases
    function ipmitool-101 --wraps ipmitool --description "ipmitool wrapper for ARGV1 case switching"
        ipmitool -e . -H 172.16.40.101 -U root -P calvin -I lanplus $argv
    end
    # ------------------------------------------------------ #
end
