#!/bin/sh

redshift -v -o -P -c \
    /etc/redshift.d/redshift.msync.conf | \
    sudo tee /var/log/redshift-periodic.log

