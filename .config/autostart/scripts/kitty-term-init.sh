#!/bin/sh
#
kitty --override <set_tab_title=foo> \
  --title <os window name> \
  --detach \
  --single-instance \
  --instance-group init-term \
  --start-as fullscreen
#
